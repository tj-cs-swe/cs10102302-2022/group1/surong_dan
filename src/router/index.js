import Vue from 'vue'
import VueRouter from 'vue-router'
import home from '../views/home.vue'
import about from '../views/about.vue'
import login from '../views/login.vue'
import register from '../views/register.vue'
import network from '../views/network.vue'
import test from '../views/test.vue'
import {reqisLogin} from "@/api";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'about',
    component: about,
    meta: {
      requireLogin: false,
    }
  },
  {
    path: '/home',
    name: 'home',
    component: home,
    meta: {
      requireLogin: true,
    }
  },
  {
    path: '/login',
    name: 'login',
    component: login,
    meta: {
      requireLogin: false,
    }
  },
  {
    path: '/register',
    name: 'register',
    component: register,
    meta: {
      requireLogin: false,
    }
  },
  {
    path: '/network',
    name: 'network',
    component: network,
    meta: {
      requireLogin: true,
    }
  },
  {
    path: '/test',
    name: 'test',
    component: test,
    meta: {
      requireLogin: false,
    }
  }

]

const router = new VueRouter({
  routes
})
function getCookie(name) {
  // 拆分 cookie 字符串
  var cookieArr = document.cookie.split(";");
 
  // 循环遍历数组元素
  for(var i = 0; i < cookieArr.length; i++) {
      var cookiePair = cookieArr[i].split("=");
     
      /* 删除 cookie 名称开头的空白并将其与给定字符串进行比较 */
      if(name == cookiePair[0].trim()) {
          // 解码cookie值并返回
          return decodeURIComponent(cookiePair[1]);
      }
  }
  // 如果未找到，则返回null
  return null;
}
//全局守卫：前置守卫（在路由跳转之间进行判断）
router.beforeEach(async(to, from, next) => {
  //to:获取到要跳转到的路由信息
  //from：获取到从哪个路由跳转过来来的信息
  //next: next() 放行  next(path) 放行  
  //方便测试 统一放行
 //  next();
  //用户登录了
  console.log("before each");
  if(to.meta.requireLogin){
    // console.log("cookie",document.cookie)
    // const res=getCookie("session");
    const islogin=await reqisLogin()
    if(islogin)
      next();
    else{
      console.log("else")
      router.push("/login");
    }
      
    console.log("身份检测");
  }else{
     next();
  }
});
export default router
