import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import home from "./home";
import login from "./login";
import register from "./register";
import about from "./about";
export default new Vuex.Store({
  modules:{
    home,
    login,
    register,
    about
  }
})




