import { reqSaveProjInfo } from "@/api"

const state = {
    cur_projectid:0
};
const mutations = {

};
const actions = {
    async CreateProject({commit},user_info){
        let res = await reqSaveProjInfo(user_info)
        state.cur_projectid = res["project_id"]
        return 'ok';
    },
    // saveProjectid(project_id){
    //     this.state.cur_projectid = project_id
    //     return 'ok';
    // }
};
const getters={
};

export default{
    namespaced:true,
    state,
    actions,
    mutations,
    getters,
}
