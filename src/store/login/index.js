import { reqLogin } from "@/api"


const state = {
    isLogin:false,
    LoginData:{}
};
const mutations = {
    SUBMITLOGIN(state,res){
        state.LoginData = res
        state.isLogin=true;
    }
};
const actions = {
    async SubmitLogin({commit},user_info){
        let res = await reqLogin(user_info)
        commit('SUBMITLOGIN',res)
        return 'ok';
    }
};
const getters={
    GetLoginRes(state){
        return state.LoginData
    }
};

export default {
    namespaced:true,
    state,
    actions,
    mutations,
    getters,
}
