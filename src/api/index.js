import requests from "./require";

// 获取数据示例
export const reqLogin = (params) => requests({ url: "/users/login", method: 'post', data: params });

export const reqRegister = (params) => requests({ url: '/users/register', method: 'post', data: params });

export const reqLogout = () => requests({ url: '/users/logout', method: 'post' });

export const reqFindPwd = () => requests({ url: '/users/findback', method: 'post' });

export const reqVerified = (params) => requests({ url: '/users/get_verified', method: 'post', data: params });

export const reqSaveProjInfo = (params) => requests({ url: '/projects/save_projinfo', method: 'post', data: params });

export const reqCopyProj = (params) => requests({ url: '/projects/copy_proj', method: 'post', data: params });

export const reqDelProj = (params) => requests({ url: '/projects/delete_proj', method: 'post', data: params });

export const reqProjList = () => requests({ url: '/projects/getlist', method: 'get' });

export const reqProjInfo = (params) => requests({ url: '/projects/getproj', method: 'post', data: params });

export const reqGetDefList=()=>requests({ url: '/projects/get_def_module', method: 'get'});

export const reqAddCustomModule=(params)=>requests({ url: '/projects/add_cus_md', method: 'post',data:params});

export const reqGetCustomModule=()=>requests({ url: '/projects/get_custom_module', method: 'get'});

export const reqSaveStructure = (params) => requests({ url: '/projects/save_structure', method: 'post', data: params });

export const reqisLogin=()=>requests({ url: 'users/isLogin', method: 'get'});
