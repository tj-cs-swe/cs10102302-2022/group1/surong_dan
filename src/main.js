import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import store from './store'
// 点云图
import VueParticles from 'vue-particles'
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';

import contentmenu from 'v-contextmenu'
import 'v-contextmenu/dist/index.css'

Vue.use(contentmenu)
Vue.config.productionTip = false

Vue.use(ViewUI);
Vue.use(ElementUI)
Vue.use(VueParticles)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
